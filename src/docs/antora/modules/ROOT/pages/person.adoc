= Person
Jorge Aguilera <jorge.aguilera@puravida-software.com>
:description: Groogle People DSL
:keyworks: groovy, google, groogle, dsl, people

[source,java]
----
interface WithPerson {
    Person person()

    WithPerson addresses(Address...s)
    WithPerson biographies(Biography...s)
    WithPerson birthdays(Birthday... s)
    WithPerson emailAddresses(EmailAddress... s)
    WithPerson events(Event... s)
    WithPerson genders(Gender... s)
    WithPerson imClients(ImClient... s)
    WithPerson interests(Interest... s)
    WithPerson locales(Locale... s)
    WithPerson names(Name... s)
    WithPerson nicknames(Nickname... s)
    WithPerson occupations(Occupation... s)
    WithPerson organizations(Organization... s)
    WithPerson phoneNumbers(PhoneNumber... s)
    WithPerson relations(Relation... s)
    WithPerson residences(Residence... s)
    WithPerson sipAddresses(SipAddress... s)
    WithPerson urls(Url... s)
    WithPerson userDefined(UserDefined... s)
}
----

Así pues podemos modificar cualquiera de los atributos anteriores y el DSL actualizará el contacto con los cambios


== Create

Para crear un contacto usaremos `createPerson` y usaremos el DSL para especificar los campos que queremos incluir
en el contacto a crear:

[source,java]
----
peopleService.createPerson( w->{
    w.addNickname( n->
        n
            .type("DEFAULT")
            .value("pelele")
    );
});
----

== Update

Mediante el DSL tenemos varias formas de recuperar un contacto y así poder modificar sus atributos. Por ejemplo
con `eachPeople` podemos realizar una búsqueda de todos ellos y por cada uno ejecutar una acción con `WithPerson`

[source,java]
----
peopleService.withPerson(first, w->{
    w.addAddress( a->
        a
            .country("Spain")
            .city("Madrid")
    );
});
----

Una vez que salimos del DSL este detectará si ha habido cambios en el contacto e invocará a Google para su actualización

== Delete

Al igual que actualizamos un contacto podemos indicar que queremos su borrado mediante `delete`

[source,java]
----
peopleService.withPerson(first, w->{
    w.delete();
});
----

WARNING: NO se puede actualizar y borrar un contacto a la vez

