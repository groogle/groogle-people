package com.puravida.groogle.impl

import com.google.api.services.people.v1.model.Address
import com.google.api.services.people.v1.model.Biography
import com.google.api.services.people.v1.model.Birthday
import com.google.api.services.people.v1.model.EmailAddress
import com.google.api.services.people.v1.model.Event
import com.google.api.services.people.v1.model.Gender
import com.google.api.services.people.v1.model.ImClient
import com.google.api.services.people.v1.model.Interest
import com.google.api.services.people.v1.model.Locale
import com.google.api.services.people.v1.model.Name
import com.google.api.services.people.v1.model.Nickname
import com.google.api.services.people.v1.model.Occupation
import com.google.api.services.people.v1.model.Organization
import com.google.api.services.people.v1.model.Person
import com.google.api.services.people.v1.model.PhoneNumber
import com.google.api.services.people.v1.model.Relation
import com.google.api.services.people.v1.model.Residence
import com.google.api.services.people.v1.model.SipAddress
import com.google.api.services.people.v1.model.Url
import com.google.api.services.people.v1.model.UserDefined
import com.puravida.groogle.PeopleService
import groovy.transform.CompileStatic

import java.util.function.Consumer

@CompileStatic
class WithPersonSpec implements PeopleService.WithPerson{

    com.google.api.services.people.v1.PeopleService service
    Person person

    protected List dirty =[]

    boolean isDirty(){
        dirty.size()
    }

    Person person(){
        person
    }

    @Override
    PeopleService.WithPerson addresses(Address... s) {
        person.addresses = s as List<Address>
        dirty.add 'addresses'
        this
    }

    @Override
    PeopleService.WithPerson biographies(Biography... s) {
        person.biographies = s as List<Biography>
        dirty.add 'biographies'
        this
    }

    @Override
    PeopleService.WithPerson birthdays(Birthday... s) {
        person.birthdays = s as List<Birthday>
        dirty.add 'birthdays'
        this
    }

    @Override
    PeopleService.WithPerson emailAddresses(EmailAddress... s) {
        person.emailAddresses = s as List<EmailAddress>
        dirty.add 'emailAddresses'
        this
    }

    @Override
    PeopleService.WithPerson events(Event... s) {
        person.events = s as List<Event>
        dirty.add 'events'
        this
    }

    @Override
    PeopleService.WithPerson genders(Gender... s) {
        person.genders = s as List<Gender>
        dirty.add 'genders'
        this
    }

    @Override
    PeopleService.WithPerson imClients(ImClient... s) {
        person.imClients = s as List<ImClient>
        dirty.add 'imClients'
        this
    }

    @Override
    PeopleService.WithPerson interests(Interest... s) {
        person.interests = s as List<Interest>
        dirty.add 'interests'
        this
    }

    @Override
    PeopleService.WithPerson locales(Locale... s) {
        person.locales = s as List<Locale>
        dirty.add 'locales'
        this
    }

    @Override
    PeopleService.WithPerson names(Name... s) {
        person.names = s as List<Name>
        dirty.add 'names'
        this
    }

    @Override
    PeopleService.WithPerson nicknames(Nickname... s) {
        person.nicknames = s as List<Nickname>
        dirty.add 'nicknames'
        this
    }

    @Override
    PeopleService.WithPerson occupations(Occupation... s) {
        person.occupations = s as List<Occupation>
        dirty.add 'occupations'
        this
    }

    @Override
    PeopleService.WithPerson organizations(Organization... s) {
        person.organizations = s as List<Organization>
        dirty.add 'organizations'
        this
    }

    @Override
    PeopleService.WithPerson phoneNumbers(PhoneNumber... s) {
        person.phoneNumbers = s as List<PhoneNumber>
        dirty.add 'phoneNumbers'
        this
    }

    @Override
    PeopleService.WithPerson relations(Relation... s) {
        person.relations = s as List<Relation>
        dirty.add 'relations'
        this
    }

    @Override
    PeopleService.WithPerson residences(Residence... s) {
        person.residences = s as List<Residence>
        dirty.add 'residences'
        this
    }

    @Override
    PeopleService.WithPerson sipAddresses(SipAddress... s) {
        person.sipAddresses = s as List<SipAddress>
        dirty.add 'sipAddresses'
        this
    }

    @Override
    PeopleService.WithPerson urls(Url... s) {
        person.urls = s as List<Url>
        dirty.add 'urls'
        this
    }

    @Override
    PeopleService.WithPerson userDefined(UserDefined... s) {
        person.userDefined = s as List<UserDefined>
        dirty.add 'userDefined'
        this
    }

    boolean toDelete = false
    @Override
    void delete() {
        toDelete = true
    }

    @Override
    PeopleService.WithPerson addAddress(Consumer<PeopleService.AddressDSL> consumer) {
        AddressSpec addressSpec = new AddressSpec(address: new Address())
        consumer.accept(addressSpec)
        List<Address> addresses = person.getAddresses()
        if( !addresses )
            addresses = []
        addresses.add(addressSpec.address)
        this.addresses(addresses as Address[])
        this
    }

    @Override
    PeopleService.WithPerson removeAddress(Consumer<PeopleService.AddressDSL> consumer) {
        List<Address> addresses = person.getAddresses()
        if( !addresses )
            return this
        AddressSpec addressSpec = new AddressSpec(address: new Address())
        consumer.accept(addressSpec)
        addresses.removeAll {
            addressSpec.isEqualTo(it)
        }
        this.addresses(addresses as Address[])
        this
    }


    @Override
    PeopleService.WithPerson addNickname(Consumer<PeopleService.NicknameDSL> consumer) {
        NicknameSpec nicknameSpec = new NicknameSpec(nickname: new Nickname(type: "DEFAULT"))
        consumer.accept(nicknameSpec)
        List<Nickname> nicknames= person.getNicknames()
        if( !nicknames )
            nicknames = []
        nicknames.add(nicknameSpec.nickname)
        this.nicknames(nicknames as Nickname[])
        this
    }

    @Override
    PeopleService.WithPerson removeNickname(Consumer<PeopleService.NicknameDSL> consumer) {
        List<Nickname> nicknames = person.getNicknames()
        if( !nicknames )
            return this
        NicknameSpec nicknameSpec = new NicknameSpec(nickname: new Nickname())
        consumer.accept(nicknameSpec)
        nicknames.removeAll {
            nicknameSpec.isEqualTo(it)
        }
        this.nicknames(nicknames as Nickname[])
        this
    }


    WithPersonSpec update(){
        if( isDirty() && toDelete ){
            throw new RuntimeException("Can't update and delete to the same time")
        }
        if( isDirty() ){
            service.people()
                    .updateContact("${person.resourceName}", person)
                    .setUpdatePersonFields(dirty.join(','))
                    .execute()
        }
        if( toDelete ){
            service.people().deleteContact("${person.resourceName}").execute()
        }
        this
    }

    WithPersonSpec create(){
        if( isDirty() ){
            service.people()
                    .createContact(person)
                    .execute()
        }
        this
    }
}
